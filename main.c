/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/22 14:09:12 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/07/10 09:30:02 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <stdio.h>
#include "get_next_line.h"

int		main(int argc, char **argv)
{
	int f;
	char *line;
	int read;
	
	if (argc != 2)
		return (1);
	f = open(argv[1], O_RDONLY);
	
	while ((read = get_next_line(f, &line)))
	{
		if (read == -1)
		{
			printf("wrong input!\n");
			return (0);
		}
		printf("%s\n", line);
	}
	return (0);
}
